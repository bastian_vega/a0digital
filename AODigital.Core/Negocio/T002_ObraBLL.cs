﻿using AODigital.Core.Datos;
using AODigital.Core.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Negocio
{
	public class T002_ObraBLL
	{
		private static T002_ObraDAO _ObraDAO = new T002_ObraDAO();

		public static T002_Obra Traer(int Id)
		{

			return _ObraDAO.Traer(Id);
		}

		public static List<T002_Obra> TraerTodos()
		{

			return _ObraDAO.TraerTodos();
		}

		public static bool Guardar(T002_Obra objObra)
		{
			return _ObraDAO.Guardar(objObra);
		}
	}
}
