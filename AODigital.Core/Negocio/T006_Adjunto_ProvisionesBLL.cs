﻿using AODigital.Core.Datos;
using AODigital.Core.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Negocio
{
    public class T006_Adjunto_ProvisionesBLL
    {
        private static T006_Adjunto_ProvisionesDAO _baseDAO = new T006_Adjunto_ProvisionesDAO();


        public static long Guardar_Adjunto(G006_Adjunto_Provisiones obj)
        {

            return _baseDAO.Guardar_Adjunto(obj);
        }

        public static G006_Adjunto_Provisiones TraerAdjuntoFolio(int folio)
        {
            return _baseDAO.TraerAdjuntoFolio(folio);
        }
    }
}
