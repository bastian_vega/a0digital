﻿using AODigital.Core.Datos;
using AODigital.Core.Entidad;
using AODigital.Core.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Negocio
{
    public class T004_Gasto_IngresoBLL
    {
        private static T004_Gasto_IngresoDAO _gastoDAO = new T004_Gasto_IngresoDAO();


        public static List<G004_Gasto_Ingreso> LOB_1_Gasto_Imgreso(string obra, string periodo)
        {

            return _gastoDAO.LOB_1_Gasto_Imgreso(obra, periodo);
        }

        public static List<G004_Gasto_Ingreso_LOB2> LOB_2(string obra, string periodo, int cuenta)
        {
            return _gastoDAO.LOB_2(obra, periodo, cuenta);
        }

        public static List<G004_Gasto_Ingreso_LOB2> LOB_2_Padres(string obra, string periodo, int cuenta)
        {
            return _gastoDAO.LOB_2_Padres(obra, periodo, cuenta);
        }

        public static List<G004_Gasto_Ingreso_LOB2> LOB_2_Hijos(string obra, string periodo, int cuenta, int cuentaId)
        {
            return _gastoDAO.LOB_2_Hijos(obra, periodo, cuenta, cuentaId);
        }

        public static List<G004_Gasto_Ingreso_LOB2> TraeListaDatoManual(string obra, string periodo, int cuenta, int cuentaId)
        {
            return _gastoDAO.TraeListaDatoManual(obra, periodo, cuenta, cuentaId);
        }
    }
}
