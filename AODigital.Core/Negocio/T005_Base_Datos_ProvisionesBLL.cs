﻿using AODigital.Core.Datos;
using AODigital.Core.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Negocio
{
    public class T005_Base_Datos_ProvisionesBLL
    {
        private static T005_Base_Datos_ProvisionesDAO _baseDAO = new T005_Base_Datos_ProvisionesDAO();


        public static long Guardar_Provision_Manual(G005_Base_Datos_Provisiones obj)
        {

            return _baseDAO.Guardar_Provision_Manual(obj);
        }

        public static long TraerTotalMonto(string obra, string periodo, int cuenta_costo, long cuenta)
        {
            return _baseDAO.TraerTotalMonto(obra,periodo,cuenta_costo,cuenta);
        }

        public static long TraerTotalMontoLob1(string obra, string periodo, long cuenta_costo)
        {
            return _baseDAO.TraerTotalMontoLob1(obra, periodo, cuenta_costo);
        }

        public static List<G005_Informe> TraerInforme(string obra, string periodo)
        {
            return _baseDAO.TraerInforme(obra, periodo);
        }
    }
}
