﻿using AODigital.Core.Datos;
using AODigital.Core.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Negocio
{
	public class T001_BaseBLL
	{
		private static T001_BaseDAO _BaseDAO = new T001_BaseDAO();

		public static T001_Base Traer(int Id)
		{

			return _BaseDAO.Traer(Id);
		}

		public static List<T001_Base> TraerTodos()
		{

			return _BaseDAO.TraerTodos();
		}

		public static bool Guardar(T001_Base objBase)
		{
			return _BaseDAO.Guardar(objBase);
		}
	}
}
