﻿using AODigital.Core.Datos;
using AODigital.Core.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Negocio
{
	public class T003_PeriodoBLL
	{
		private static T003_PeriodoDAO _PeriodoDAO = new T003_PeriodoDAO();


		public static T003_Periodo Traer(int Id)
		{

			return _PeriodoDAO.Traer(Id);
		}

		public static List<T003_Periodo> TraerTodos()
		{

			return _PeriodoDAO.TraerTodos();
		}

		public static bool Guardar(T003_Periodo objPeriodo)
		{
			return _PeriodoDAO.Guardar(objPeriodo);
		}
	}
}
