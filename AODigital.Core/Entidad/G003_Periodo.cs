﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Entidad
{
	public class G003_Periodo
	{
		public long Id { get; set; }
		public string Periodo { get; set; }
	}
}
