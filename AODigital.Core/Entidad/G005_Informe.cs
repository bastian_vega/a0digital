﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Entidad
{
    public class G005_Informe
    {
        public string Empresa { get; set; }
        public string G_I { get; set; }
        public string Cuenta_de_Costo { get; set; }
        public string Descripción_Cuenta { get; set; }
        public string Descripción_Insumo { get; set; }
        public string fecha { get; set; }
        public string Movimiento { get; set; }
        public string Tipo_Documento { get; set; }
        public string Numero { get; set; }
        public string Glosa { get; set; }
        public string Monto { get; set; }
       
        public string Cuenta_Insumo { get; set; }
        public string Tipo { get; set; }
        public string TipoDato { get; set; }

    }
}
