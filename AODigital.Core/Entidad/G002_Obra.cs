﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Entidad
{
	public class G002_Obra
	{
		public long Id { get; set; }
		public string Obra { get; set; }
		public Nullable<System.DateTime> Eliminado { get; set; }
	}
}
