﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Entidad
{
    public class G005_Base_Datos_Provisiones
    {
        public long Folio { get; set; }
        public int DatoProvision { get; set; }
        public string Glosa { get; set; }
        public string DatoGlosa { get; set; }
        public DateTime Fecha { get; set; }
        public string Periodo { get; set; }
        public string Empresa { get; set; }
        public string Obra { get; set; }
        public string Cuenta { get; set; }
        public string Cuenta_Costo { get; set; }
        public string TipoDocumento { get; set; }
        public string Numero { get; set; }
        public string Insumo { get; set; }

    }
}
