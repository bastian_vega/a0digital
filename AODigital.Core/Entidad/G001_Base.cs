﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Entidad
{
	public class G001_Base
	{
		public long Id { get; set; }
		public long Cuenta_de_Costo { get; set; }
		public string Obra { get; set; }
		public string Descripcion { get; set; }
		public long Comprobante { get; set; }
		public string Referencia { get; set; }
		public string Factura { get; set; }
		public string docdesc { get; set; }
		public string NUMERO { get; set; }
		public string Modulo { get; set; }
		public DateTime fecha { get; set; }
		public string Periodo { get; set; }
		public string Glosa { get; set; }
		public string Monto { get; set; }
		public string Movimiento { get; set; }
		public string Cuenta { get; set; }
		public string User5 { get; set; }
		public string DocType { get; set; }
		public string acct { get; set; }
		public string SINOC { get; set; }
		public string ESTADOFLUJO { get; set; }
		public string FOLIOUNICO { get; set; }
		public string Insumo_Desc { get; set; }
		public string ObraCargo { get; set; }
		public string NOTE { get; set; }
		public string ReportNumber { get; set; }
		public string Tipo_doc { get; set; }
		public string Tipo { get; set; }
		public string NUMOC { get; set; }
		public string NOMOC { get; set; }
	}
}
