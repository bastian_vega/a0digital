﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Entidad
{
    public class G004_Gasto_Ingreso_LOB2
    {
   
        public string Fecha { get; set; }
        public string Movimiento { get; set; }
        public string Tipo_Documento { get; set; }
        public string Numero { get; set; }
        public string Gosa { get; set; }
        public string Monto { get; set; }
        public string Cuenta2_Id { get; set; }

        public string TipoOrdRen { get; set; }

        public string NOMOC { get; set; }
        public string NUMOC { get; set; }
        public string ReportNumber { get; set; }
        public string NOTE { get; set; }
        public string Detalle { get; set; }




    }
}
