﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Entidad
{
    public class G006_Adjunto_Provisiones
    {
        public long IdAdjunto { get; set; }
        public long IdProvision { get; set; }
        public string NombreArchivo { get; set; }
        public string Mime { get; set; }
    }
}
