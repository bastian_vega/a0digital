﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Entidad
{
    public class G004_Gasto_Ingreso
    {
        public long? Cuenta { get; set; }
        public string Descripcion { get; set; }
        public string G_I { get; set; }
        public int? Orden { get; set; }
        public string Total { get; set; }
        public string Nombre_Completo { get; set; }
    }
}


