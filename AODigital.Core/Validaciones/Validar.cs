﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Tributa.Core.Validaciones
{
	public class Validar
	{
		private static readonly Regex Fecha = new Regex("^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](20)[2][0-1]$", RegexOptions.Compiled);
		private static readonly Regex Negativo = new Regex("^-([0]{1}[0 - 9] +|[1 - 9]{1}[0-9]*[0-9]+|[0-9]+|0)$", RegexOptions.Compiled);

		//Expresiones regulares para validar contraseña
		private static readonly Regex Longitud = new Regex(@"^\w{6,15}\b", RegexOptions.Compiled);
		private static readonly Regex Numeros = new Regex(@"\d", RegexOptions.Compiled);
		private static readonly Regex Mayusculas = new Regex(@"[A-Z]", RegexOptions.Compiled);

		//Validaciones genericas
		//Metodo que valida si la clave tiene al menos un numero, una mayuscula y no tiene patrones de teclado
		public static bool ValidaClave(string clave)
		{
			bool errorFlag = false;
			int error = 0;
			if (!Longitud.Match(clave).Success)
			{
				error = 1;
				errorFlag = true;
			}
			else if (!Numeros.Match(clave).Success)
			{
				error = 2;
				errorFlag = true;
			}
			else if (!Mayusculas.Match(clave).Success)
			{
				error = 3;
				errorFlag = true;
			}

			return errorFlag;
		}

		public static bool EsFechaValida(string fecha) {
			return Fecha.Match(fecha).Success;
		}


        //Metodo para validar si un campo està vacio
        public static bool EsVacioNulo(string campo)
		{
			return campo.Trim() == string.Empty || campo == null || campo == "-";
		}

        public static string ConverirCeroString(string campo)
        {
            string valor = campo;
            if (campo == string.Empty || campo == null || campo == "-")
            {
                valor = "0";
            }
            return valor;
        }

		//Metodo para validar si un campo es 0
		public static bool EsCero(long campo1)
		{
			return campo1 == 0;
		}

        //Metodo para validar si un campo es 0
        public static bool EsCeroString(string campo1)
        {
            return campo1 == "0";
        }

        //Metodo para validar si un campo es menor a otro
        public static bool EsMenorA(int campo1, int campo2)
		{
			return campo1 < campo2;
		}

		//Metodo que valida si dos campos son igualess
		public bool EsIgual(int campo4, int campo5)
		{
			return campo4 == campo5;

		}

		//Metodo para validar si un campo es un numero entero
		public static bool EsEntero(string campo)
		{
			bool respuesta = false;

			string numString = campo;
			long number1;

			bool canConvert = long.TryParse(numString, out number1);
			if (canConvert)
				respuesta = true;

            return respuesta;
		}
		//Metodo para validar rut chileno
		public static bool ValidaRut(string rut)
		{
			rut = rut.Replace(".", "").ToUpper();
			Regex expresion = new Regex("^([0-9]+-[0-9K])$");
			string dv = rut.Substring(rut.Length - 1, 1);
			if (!expresion.IsMatch(rut))
			{
				return false;
			}
			char[] charCorte = { '-' };
			string[] rutTemp = rut.Split(charCorte);
			if (dv != Digito(int.Parse(rutTemp[0])))
			{
				return false;
			}
			return true;
		}
		//Metodo para validar digito verificador
		public static string Digito(int rut)
		{
			int suma = 0;
			int multiplicador = 1;
			while (rut != 0)
			{
				multiplicador++;
				if (multiplicador == 8)
					multiplicador = 2;
				suma += (rut % 10) * multiplicador;
				rut = rut / 10;
			}
			suma = 11 - (suma % 11);
			if (suma == 11)
			{
				return "0";
			}
			else if (suma == 10)
			{
				return "K";
			}
			else
			{
				return suma.ToString();
			}
		}

		//Metodo que valida el rut y el digito verificador por separado
		public static bool ValidaRut(string rut, string dv)
		{
			return ValidaRut(rut + "-" + dv);
		}

		//Metodo que valida si el correo es valido
		public static bool CorreoValido(string correo)
		{
			try
			{
				MailAddress email = new MailAddress(correo);

				return true;
			}
			catch (FormatException)
			{
				return false;
			}
		}

		//Metodo que comprueba si el campo tiene maximo 10 caracteres
		public static bool Largo10(string campo)
		{
			return campo.Length <= 10;
		}

		//Metodo que comprueba si el campo tiene maximo 15 caracteres
		public static bool Largo15(string campo)
		{
			return campo.Length <= 15;
		}

		//Metodo que comprueba si el campo tiene maximo 20 caracteres
		public static bool Largo20(string campo)
		{
			return campo.Length <= 20;
		}

		//Metodo que comprueba si el campo tiene maximo 30 caracteres
		public static bool Largo30(string campo)
		{
			return campo.Length <= 30;
		}

		//Metodo que comprueba si el campo tiene maximo 4 caracteres
		public static bool Largo4(string campo)
		{
			return campo.Length <= 4;
		}

		//Metodo que comprueba si el campo tiene maximo 6 caracteres
		public static bool Largo6(string campo)
		{
			return campo.Length <= 6;
		}

		//Metodo que comprueba si el campo tiene maximo 100 caracteres
		public static bool Largo1(string campo)
		{
			return campo.Length <= 1;
		}

		//Metodo que comprueba si el campo tiene maximo 100 caracteres
		public static bool Largo100(string campo)
		{
			return campo.Length <= 100;
		}

		//Metodo que valida si el campo es igual al summatorio de otro campo
		public static bool Sumatorio(int campo, int suma)
		{
			return campo == suma;
		}

		//Metodo que valida si el campo es una fecha valida en formato dia, mes, año
		public static bool EsFecha(int campo)
		{
			bool Valido;
			try
			{
				DateTime DT = DateTime.Parse(campo.ToString("dd MMMM, yyyy"));
				Valido = true;
			}
			catch (Exception e)
			{
				Valido = false;
			}

			return Valido;
		}

		//Separador de miles
		public static string SeparadorM(long campo)
		{
			NumberFormatInfo formato = new CultureInfo("es-cl").NumberFormat;

			formato.CurrencyGroupSeparator = ".";
			formato.NumberDecimalSeparator = ",";
			formato.CurrencySymbol = " ";

			var campo_con = campo.ToString("C", formato);

			return campo_con;
		}

		public static string QuitarPunto(string campo)
		{

			var SinPunto = campo.Replace(".", "").Replace("!","").Trim();
			return SinPunto;
		}

        public static string QuitaCaracteresEsp(string Texto)
        {
			string palabaSinTildes = Regex.Replace(Texto.Normalize(NormalizationForm.FormD), @"[^a-zA-z0-9 ]+", "");
			var SinEnhe = palabaSinTildes.Replace("ñ", "n").Trim();
			string TextoLimpio = Regex.Replace(SinEnhe, "[^ 0-9A-Za-z]", "", RegexOptions.None);
            return TextoLimpio;
        }

		public static string QuitaTilde(string Texto)
		{
			string palabaSinTildes = Regex.Replace(Texto.Normalize(NormalizationForm.FormD), @"[^a-zA-z0-9 ]+", "");
			return palabaSinTildes;
		}

		public static string QuitarEnhe(string campo)
		{
			var SinEnhe = campo.Replace("ñ", "n").Trim();
			return SinEnhe;
		}

		public static bool EsNegativo(string campo)
		{
			return Negativo.Match(campo).Success;
		}
	}

}

