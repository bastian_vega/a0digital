﻿using AODigital.Core.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Datos
{
	public class T002_ObraDAO
	{
		private ICIL_DATAEntities _dbContext;

		public T002_ObraDAO()
		{
			if (_dbContext == null)
				_dbContext = new ICIL_DATAEntities();
		}

		internal T002_Obra Traer(int Id)
		{
			var entidad = (T002_Obra)null;

			var query = from obra in _dbContext.T002_Obra
						where obra.Id == Id
						select obra;

			entidad = query.FirstOrDefault();

			return entidad;
		}

		internal List<T002_Obra> TraerTodos()
		{
			var lista = new List<T002_Obra>();

			var query = from obra in _dbContext.T002_Obra
                        orderby obra.Obra
							//(!perfil.HasValue | usuario.Perfil == perfil) && (!area.HasValue | usuario.Area == area)
						select obra;
			lista = query.ToList();
			return lista;
		}

		internal bool Guardar(T002_Obra objObra)
		{
			var guardado = false;

			if (_dbContext.T002_Obra.Any(o => o.Id == objObra.Id))
				_dbContext.Entry(objObra).State = System.Data.Entity.EntityState.Modified;
			else
				_dbContext.Entry(objObra).State = System.Data.Entity.EntityState.Added;

			var contador = _dbContext.SaveChanges();
			guardado = contador > 0;

			return guardado;
		}
	}
}
