﻿using AODigital.Core.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Datos
{
	public class T001_BaseDAO
	{
		private ICIL_DATAEntities _dbContext;

		public T001_BaseDAO()
		{
			if (_dbContext == null)
				_dbContext = new ICIL_DATAEntities();
		}

		internal T001_Base Traer(int Id)
		{
			var entidad = (T001_Base)null;

			var query = from bas in _dbContext.T001_Base
						where bas.Id == Id
						select bas;

			entidad = query.FirstOrDefault();

			return entidad;
		}

		internal List<T001_Base> TraerTodos()
		{
			var lista = new List<T001_Base>();

			var query = from bas in _dbContext.T001_Base

							//(!perfil.HasValue | usuario.Perfil == perfil) && (!area.HasValue | usuario.Area == area)
						select bas;
			lista = query.ToList();
			return lista;
		}

		internal bool Guardar(T001_Base objBase)
		{
			var guardado = false;

			if (_dbContext.T001_Base.Any(o => o.Id == objBase.Id))
				_dbContext.Entry(objBase).State = System.Data.Entity.EntityState.Modified;
			else
				_dbContext.Entry(objBase).State = System.Data.Entity.EntityState.Added;

			var contador = _dbContext.SaveChanges();
			guardado = contador > 0;

			return guardado;
		}
	}
}
