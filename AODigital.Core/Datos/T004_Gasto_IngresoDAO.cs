﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AODigital.Core.Entidad;
using AODigital.Core.Modelo;
using Tributa.Core.Validaciones;

namespace AODigital.Core.Datos
{
    public class T004_Gasto_IngresoDAO
    {
        private ICIL_DATAEntities _dbContext;

        public T004_Gasto_IngresoDAO()
        {
            if (_dbContext == null)
                _dbContext = new ICIL_DATAEntities();
        } 


        internal List<G004_Gasto_Ingreso> LOB_1_Gasto_Imgreso(string obra, string periodo)
        {
            var listaNodo = new List<G004_Gasto_Ingreso>();


            var procedure = _dbContext.LOB_1_Gasto_Imgreso(obra,periodo);


            foreach (var item in procedure)
            {
                G004_Gasto_Ingreso objTotal = new G004_Gasto_Ingreso();
                objTotal.Cuenta = item.Cuenta;
                objTotal.Descripcion = item.Descripcion;
                objTotal.G_I = item.G_I;
                objTotal.Orden = item.Orden;
                objTotal.Total = Validar.SeparadorM(item.Total);
                objTotal.Nombre_Completo = item.Cuenta + " " + item.Descripcion;




                listaNodo.Add(objTotal);
            }


            return listaNodo;
        }

        internal List<G004_Gasto_Ingreso_LOB2> TraeListaDatoManual(string obra, string periodo, int cuenta, int cuentaId)
        {
            var listaNodo = new List<G004_Gasto_Ingreso_LOB2>();


            var procedure = _dbContext.Lista_Dato_Manual(obra, periodo, cuenta, cuentaId);


            foreach (var item in procedure)
            {
                G004_Gasto_Ingreso_LOB2 objTotal = new G004_Gasto_Ingreso_LOB2();
                objTotal.Fecha = item.Fecha;
                objTotal.Gosa = item.Glosa;
                objTotal.Monto = Validar.SeparadorM((long)item.Monto);
                objTotal.Movimiento = item.Movimiento+"";
                objTotal.Numero = item.Numero+"";
                objTotal.Tipo_Documento = item.Tipo_Documento;
                objTotal.Cuenta2_Id = item.Cuenta;
                objTotal.Detalle = "+";

                //if (item.NUMOC != null)
                //{
                //    objTotal.NUMOC = item.NUMOC + "";
                    objTotal.NOMOC = item.NOMOC + "";
                   objTotal.TipoOrdRen = "Provision";


                //}
                //else
                //{
                //    if (item.ReportNumber != null)
                //    {
                //        objTotal.NUMOC = item.ReportNumber + "";
                //        objTotal.NOMOC = item.NOTE + "";
                //        objTotal.TipoOrdRen = "Rendicion";
                //    }
                //    else
                //    {
                //        objTotal.NOMOC = item.docdesc + "";
                //        objTotal.TipoOrdRen = "docdesc";
                //    }
                //}







                //objTotal.ReportNumber = item.ReportNumber;
                //objTotal.NOTE = item.NOTE;
                //objTotal.Detalle = item.Detalle;

                listaNodo.Add(objTotal);
            }


            return listaNodo;
        }

        internal List<G004_Gasto_Ingreso_LOB2> LOB_2_Hijos(string obra, string periodo, int cuenta, int cuentaId)
        {
            var listaNodo = new List<G004_Gasto_Ingreso_LOB2>();


            var procedure = _dbContext.LOB2_HIJO(obra, periodo, cuenta,cuentaId);


            foreach (var item in procedure)
            {
                G004_Gasto_Ingreso_LOB2 objTotal = new G004_Gasto_Ingreso_LOB2();
                objTotal.Fecha = item.Fecha;
                objTotal.Gosa = item.Glosa;
                objTotal.Monto = Validar.SeparadorM(long.Parse(item.Monto));
                objTotal.Movimiento = item.Movimiento;
                objTotal.Numero = item.Numero;
                objTotal.Tipo_Documento = item.Tipo_Documento;
                objTotal.Cuenta2_Id = item.Cuenta;


                if (item.NUMOC != null)
                {
                    objTotal.NUMOC = item.NUMOC;
                    objTotal.NOMOC = item.NOMOC;
                    objTotal.TipoOrdRen = "Orden";
                    
                  
                }
                else
                {
                    if (item.ReportNumber != null)
                    {
                        objTotal.NUMOC = item.ReportNumber;
                        objTotal.NOMOC = item.NOTE;
                        objTotal.TipoOrdRen = "Rendicion";
                    }
                    else
                    {
                        objTotal.NOMOC = item.docdesc;
                        objTotal.TipoOrdRen = "docdesc";
                    }
                }

               





                objTotal.ReportNumber = item.ReportNumber;
                objTotal.NOTE = item.NOTE;
                objTotal.Detalle = item.Detalle;

                listaNodo.Add(objTotal);
            }


            return listaNodo;
        }

        internal List<G004_Gasto_Ingreso_LOB2> LOB_2_Padres(string obra, string periodo, int cuenta)
        {
            var listaNodo = new List<G004_Gasto_Ingreso_LOB2>();


            var procedure = _dbContext.LOB2_PADRE(obra, periodo, cuenta);


            foreach (var item in procedure)
            {
                G004_Gasto_Ingreso_LOB2 objTotal = new G004_Gasto_Ingreso_LOB2();
                objTotal.Fecha = item.Fecha+"";
               
                    
                objTotal.Monto = Validar.SeparadorM((long)item.Total_Lineas);
                objTotal.Movimiento = item.Movimiento+"";
                objTotal.Numero = item.Numero+"";
                objTotal.Tipo_Documento = item.Tipo_Documento+"";
                objTotal.Cuenta2_Id = item.Cuenta;
                objTotal.Gosa = item.Insumo;



                listaNodo.Add(objTotal);
            }


            return listaNodo;
        }

        internal List<G004_Gasto_Ingreso_LOB2> LOB_2(string obra, string periodo,int cuenta)
        {
            var listaNodo = new List<G004_Gasto_Ingreso_LOB2>();


            var procedure = _dbContext.LOB2(obra, periodo,cuenta);


            foreach (var item in procedure)
            {
                G004_Gasto_Ingreso_LOB2 objTotal = new G004_Gasto_Ingreso_LOB2();
                objTotal.Fecha = item.Fecha;
                objTotal.Gosa = item.Glosa;
                objTotal.Monto = Validar.SeparadorM((long)item.Monto);
                objTotal.Movimiento = item.Movimiento;
                objTotal.Numero = item.Numero;
                objTotal.Tipo_Documento = item.Tipo_Documento;
                objTotal.Cuenta2_Id = item.Cuenta;

                objTotal.NOMOC = item.NOMOC;
objTotal.NUMOC = item.NUMOC;
                objTotal.ReportNumber = item.ReportNumber;
                objTotal.NOTE= item.NOTE;

                listaNodo.Add(objTotal);
            }


            return listaNodo;
        }
    }
}
