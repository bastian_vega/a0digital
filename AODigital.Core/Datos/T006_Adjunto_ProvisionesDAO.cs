﻿using AODigital.Core.Entidad;
using AODigital.Core.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Datos
{
    public class T006_Adjunto_ProvisionesDAO
    {
        private ICIL_DATAEntities _dbContext;

        public T006_Adjunto_ProvisionesDAO()
        {
            if (_dbContext == null)
                _dbContext = new ICIL_DATAEntities();
        }

        internal long Guardar_Adjunto(G006_Adjunto_Provisiones obj)
        {
            long guardado = 0;


            var procedure = _dbContext.Guardar_Adjunto(obj.IdProvision, obj.Mime, obj.NombreArchivo).FirstOrDefault();



            if (procedure > 0)
            {
                guardado = (long)procedure;
            }


            return guardado;
        }

        internal G006_Adjunto_Provisiones TraerAdjuntoFolio(int folio)
        {
            var listaNodo = new List<G006_Adjunto_Provisiones>();


            var procedure = _dbContext.TraerAdjuntoFolio(folio);


            foreach (var item in procedure)
            {
                G006_Adjunto_Provisiones objAdjunto = new G006_Adjunto_Provisiones();
                objAdjunto.IdAdjunto = item.IdAdjunto;
                objAdjunto.IdProvision = item.IdProvision;
                objAdjunto.Mime = item.Mime;
                objAdjunto.NombreArchivo = item.NombreArchivo;
               




                listaNodo.Add(objAdjunto);
            }


            return listaNodo.FirstOrDefault();
        }
    }
}
