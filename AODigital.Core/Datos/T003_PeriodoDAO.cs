﻿using AODigital.Core.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AODigital.Core.Datos
{
	public class T003_PeriodoDAO
	{
		private ICIL_DATAEntities _dbContext;

		public T003_PeriodoDAO()
		{
			if (_dbContext == null)
				_dbContext = new ICIL_DATAEntities();
		}

		internal T003_Periodo Traer(int Id)
		{
			var entidad = (T003_Periodo)null;

			var query = from per in _dbContext.T003_Periodo
						where per.Id == Id
						select per;

			entidad = query.FirstOrDefault();

			return entidad;
		}

		internal List<T003_Periodo> TraerTodos()
		{
			var lista = new List<T003_Periodo>();

			var query = from per in _dbContext.T003_Periodo

							//(!perfil.HasValue | usuario.Perfil == perfil) && (!area.HasValue | usuario.Area == area)
						select per;
			lista = query.ToList();
			return lista;
		}

		internal bool Guardar(T003_Periodo objPeriodo)
		{
			var guardado = false;

			if (_dbContext.T003_Periodo.Any(o => o.Id == objPeriodo.Id))
				_dbContext.Entry(objPeriodo).State = System.Data.Entity.EntityState.Modified;
			else
				_dbContext.Entry(objPeriodo).State = System.Data.Entity.EntityState.Added;

			var contador = _dbContext.SaveChanges();
			guardado = contador > 0;

			return guardado;
		}
	}
}
