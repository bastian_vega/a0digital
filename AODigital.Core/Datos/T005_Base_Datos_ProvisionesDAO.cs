﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AODigital.Core.Entidad;
using AODigital.Core.Modelo;
using Tributa.Core.Validaciones;

namespace AODigital.Core.Datos
{
    public class T005_Base_Datos_ProvisionesDAO
    {
        private ICIL_DATAEntities _dbContext;

        public T005_Base_Datos_ProvisionesDAO()
        {
            if (_dbContext == null)
                _dbContext = new ICIL_DATAEntities();
        }

        internal long Guardar_Provision_Manual(G005_Base_Datos_Provisiones obj)
        {
            long guardado = 0;


            var procedure = _dbContext.Guardar_Provision_Manual(obj.Cuenta,obj.Cuenta_Costo,obj.DatoGlosa,obj.DatoProvision,obj.Empresa,obj.Glosa,obj.Insumo,obj.Numero,obj.Obra,obj.Periodo,obj.TipoDocumento).FirstOrDefault();



            if (procedure > 0)
            {
                guardado = (long)procedure;
            }


            return guardado;
        }

        internal long TraerTotalMontoLob1(string obra, string periodo, long cuenta_costo)
        {
            long guardado = 0;


            var procedure = _dbContext.Total_Monto_Lob1(obra, periodo, cuenta_costo).FirstOrDefault();




            guardado = (long)procedure;



            return guardado;
        }

        internal List<G005_Informe> TraerInforme(string obra, string periodo)
        {
            var listaNodo = new List<G005_Informe>();


            var procedure = _dbContext.Descarga_Sabana_Lob(obra, periodo);


            foreach (var item in procedure)
            {
                G005_Informe objInforme = new G005_Informe();
               
                objInforme.Cuenta_de_Costo = item.Cuenta_de_Costo+"".Trim();
                objInforme.Cuenta_Insumo = item.Cuenta_Insumo.Trim();
                objInforme.Descripción_Cuenta = Validar.QuitarEnhe(Validar.QuitaTilde(item.Descripción_Cuenta.Trim()));
                objInforme.Empresa = item.Empresa.Trim();
                objInforme.fecha = DateTime.Parse(item.fecha+"".Trim()).ToString("MM/dd/yyyy");
                objInforme.Glosa = Validar.QuitarEnhe(Validar.QuitaTilde(item.Glosa.Trim()));
                objInforme.G_I = item.G_I.Trim();
                objInforme.Monto = item.Monto+"".Trim();
                objInforme.Movimiento = item.Movimiento.Trim();
                objInforme.Numero = item.Numero+"".Trim();
                objInforme.Tipo = Validar.QuitarEnhe(Validar.QuitaTilde(item.Tipo.Trim()));
                objInforme.TipoDato = item.TipoDato.Trim();
                objInforme.Tipo_Documento = Validar.QuitarEnhe(Validar.QuitaTilde(item.Tipo_Documento.Trim()));
                objInforme.Descripción_Insumo = Validar.QuitarEnhe(Validar.QuitaTilde(item.Descripción_Insumo+"".Trim()));








                listaNodo.Add(objInforme);
            }


            return listaNodo;
        }

        internal long TraerTotalMonto(string obra, string periodo, int cuenta_costo, long cuenta)
        {
            long guardado = 0;


            var procedure = _dbContext.Total_Monto_Manual(obra,periodo,cuenta_costo, cuenta).FirstOrDefault();



           
            guardado = (long)procedure;
            


            return guardado;
        }
    }
}
