﻿using AODigital.Core.Entidad;
using AODigital.Core.Negocio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Tributa.Core.Validaciones;

namespace AODigital.Web
{
    public partial class LibroDeObra : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarLob1();
				CargarObra();

			}
        }

		private void CargarObra()
		{
			var Obras = T002_ObraBLL.TraerTodos();

			DrpObra.DataSource = Obras;
			DrpObra.DataTextField = "Obra";
			DrpObra.DataValueField = "Obra";
			DrpObra.DataBind();
		}

        private void CargarLob1()
        {
			var Periodo = ddlAnio.SelectedValue+ddlMes.SelectedValue;

            var totales_lob1 = T004_Gasto_IngresoBLL.LOB_1_Gasto_Imgreso(DrpObra.SelectedValue, Periodo);
            totales_lob1 = sumarMontonManualLob1(totales_lob1);

            this.rptLOB1.DataSource = null;
            this.rptLOB1.DataSource = totales_lob1;
            this.rptLOB1.DataBind();
            CargarResultado(totales_lob1);
        }

        private void DescargarExcel()
        {
            var sb = new StringBuilder();
            var Periodo = ddlAnio.SelectedValue + ddlMes.SelectedValue;

            var lista_informe = T005_Base_Datos_ProvisionesBLL.TraerInforme(DrpObra.SelectedValue, Periodo);
            
            sb.AppendLine("Empresa; G_I; Cuenta_de_Costo; Descripcion_Cuenta; fecha; Movimiento; Tipo_Documento; Numero; Glosa; Monto; Cuenta_Insumo; Descripcion_Insumo; Tipo; TipoDato");

            foreach (var item in lista_informe)
            {
                sb.Append(item.Empresa+"" + ";");
                sb.Append(item.G_I + "" + ";");
                sb.Append(item.Cuenta_de_Costo + "" + ";");
                sb.Append(item.Descripción_Cuenta + "" + ";");
                sb.Append(item.fecha + "" + ";");
                sb.Append(item.Movimiento + "" + ";");
                sb.Append(item.Tipo_Documento + "" + ";");
                sb.Append(item.Numero + "" + ";");
                sb.Append(item.Glosa + "" + ";");
                sb.Append(item.Monto + "" + ";"); 

                sb.Append(item.Cuenta_Insumo + "" + ";");
                sb.Append(item.Descripción_Insumo + "" + ";");
                sb.Append(item.Tipo + "" + ";");
                sb.AppendLine(item.TipoDato + "" + ";");

            }


            var data = Encoding.UTF8.GetBytes(sb.ToString());
            var mem = new MemoryStream(data);

            Response.Clear();
            Response.ContentType = "text/csv; charset=UTF-8";
            Response.Charset = "UTF-8";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.AddHeader("Content-Disposition",
                       "attachment; filename=Sabana_"+ DrpObra.SelectedValue+"_"+ Periodo + ".csv");


            mem.WriteTo(Response.OutputStream);
            Response.End();

        }

        private List<G004_Gasto_Ingreso> sumarMontonManualLob1(List<G004_Gasto_Ingreso> totales_lob1)
        {
            var Periodo = ddlAnio.SelectedValue + ddlMes.SelectedValue;
            long total_manual_ingreso = 0;
            long total_manual_gasto = 0;
            foreach (var item in totales_lob1)
            {
                if (item.Cuenta != null)
                {
                    long total_manual = T005_Base_Datos_ProvisionesBLL.TraerTotalMontoLob1(DrpObra.SelectedValue, Periodo, (long)item.Cuenta);
                    switch (item.G_I)
                    {
                        case "INGRESOS":
                            total_manual_ingreso += total_manual;
                            break;
                        case "GASTOS":
                            total_manual_gasto += total_manual;
                            break;
                        default:
                            break;
                    }

                    long valoractual = long.Parse(Validar.QuitarPunto(item.Total));
                    long valor_futuro = valoractual + total_manual;

                    item.Total = Validar.SeparadorM(valor_futuro); 
                }
               

            }
            foreach (var item in totales_lob1)
            {
                if (item.Cuenta == null)
                {
                    long valoractual = long.Parse(Validar.QuitarPunto(item.Total));
                    long valor_futuro = 0;
                    switch (item.G_I)
                    {
                        case "INGRESOS":

                            valor_futuro = valoractual + total_manual_ingreso;
                            item.Total = Validar.SeparadorM(valor_futuro);
                            break;
                        case "GASTOS":
                            valor_futuro = valoractual + total_manual_gasto;
                            item.Total = Validar.SeparadorM(valor_futuro);
                            break;
                        default:
                            break;
                    }
                }
            }

            return totales_lob1;
        }

        private void CargarResultado(List<G004_Gasto_Ingreso> totales_lob1)
        {
            long resultado = 0;
            long gasto = 0;
            long ingreso = 0;
            foreach (var item in totales_lob1)
            {
                switch (item.Descripcion)
                {
                    case "Total Ingresos":
                        ingreso = long.Parse(Validar.QuitarPunto(item.Total));
                        break;
                    case "Total Gastos":
                        gasto = long.Parse(Validar.QuitarPunto(item.Total));
                        break;

                    default:
                        break;
                }
            }

            resultado = gasto + ingreso;
            lblResultado.Text = Validar.SeparadorM(resultado) + "";
        }

        
        protected void rptLOB1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem item = e.Item; // elemento del Repeater

            if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
            {
                HiddenField hdfCuenta = (HiddenField)item.FindControl("hdfCuenta");
                LinkButton btnVer = (LinkButton)item.FindControl("btnVer");
                HtmlTableRow trLinea = (HtmlTableRow)e.Item.FindControl("trLinea");
                HtmlTableCell tdDes = (HtmlTableCell)e.Item.FindControl("tdDes");
                HtmlTableCell tdTotal = (HtmlTableCell)e.Item.FindControl("tdTotal");

                if (hdfCuenta.Value == "")
                {
                    btnVer.Visible = false;
                    trLinea.Attributes["class"] = "destacada-izq";
                    tdTotal.Attributes["style"] = "font-weight: bold;";
                    tdDes.Attributes["style"] = "font-weight: bold;";
                 
                }
              

            }
        }

		protected void DrpObra_SelectedIndexChanged(object sender, EventArgs e)
		{
			CargarLob1();
            limpiarCambio();

        }

		protected void fecha_TextChanged(object sender, EventArgs e)
		{
			CargarLob1();
            limpiarCambio();
		}

        private void limpiarCambio()
        {
            pnlVerObra.Visible = false;
        }

        protected void rptLOB1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            pnlProvision1.Visible = false;
            pnlProvision2.Visible = false;
            pnlVerObra.Visible = true;
            int cuenta = int.Parse(e.CommandArgument.ToString());
            hdfCuenta.Value = cuenta + "";
            hdfCuentaAux.Value= cuenta + "";
            var Periodo = ddlAnio.SelectedValue + ddlMes.SelectedValue;
            var ListaDato = T004_Gasto_IngresoBLL.LOB_2_Padres(DrpObra.SelectedValue, Periodo, cuenta);
            ListaDato = SumarMontoManualAPadre(ListaDato);
            CargarGrillasDinamicas(ListaDato);
            LimpiarCssGilla();
            RepeaterItem item = e.Item; // elemento del Repeater
            HiddenField hdfDescripcion = (HiddenField)item.FindControl("hdfDescripcion");
            HtmlTableRow trLinea = (HtmlTableRow)item.FindControl("trLinea");
            trLinea.Attributes["class"] = "selecionado";
            hdfObraSeleccionada.Value = hdfDescripcion.Value;
            btnDescargarSabana.Visible = true;
        }

        private List<G004_Gasto_Ingreso_LOB2> SumarMontoManualAPadre(List<G004_Gasto_Ingreso_LOB2> listaDato)
        {

            var Periodo = ddlAnio.SelectedValue + ddlMes.SelectedValue;
            foreach (var item in listaDato)
            {
                long total_manual = T005_Base_Datos_ProvisionesBLL.TraerTotalMonto(DrpObra.SelectedValue, Periodo, int.Parse(hdfCuentaAux.Value), long.Parse(item.Cuenta2_Id));


                long valoractual = long.Parse(Validar.QuitarPunto(item.Monto));
                long valor_futuro = valoractual + total_manual;

                    item.Monto =  Validar.SeparadorM(valor_futuro);
                
            }

            return listaDato;
        }

        private void LimpiarCssGilla()
        {
            foreach (RepeaterItem item in rptLOB1.Items)
            {
                HiddenField hdfCuenta = (HiddenField)item.FindControl("hdfCuenta");
                LinkButton btnVer = (LinkButton)item.FindControl("btnVer");
                HtmlTableRow trLinea = (HtmlTableRow)item.FindControl("trLinea");
                HtmlTableCell tdDes = (HtmlTableCell)item.FindControl("tdDes");
                HtmlTableCell tdTotal = (HtmlTableCell)item.FindControl("tdTotal");

                trLinea.Attributes["class"] = "";

                if (hdfCuenta.Value == "")
                {
                    btnVer.Visible = false;
                    trLinea.Attributes["class"] = "destacada-izq";
                    tdTotal.Attributes["style"] = "font-weight: bold;";
                    tdDes.Attributes["style"] = "font-weight: bold;";

                }
            }
        }

        private void CargarGrillasDinamicas(List<G004_Gasto_Ingreso_LOB2> grillasLob2)
        {
            this.rptGrillasDinamicas.DataSource = null;
            this.rptGrillasDinamicas.DataSource = grillasLob2;
            this.rptGrillasDinamicas.DataBind();
        }

       

        protected void rptGrillasDinamicas_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem item = e.Item; // elemento del Repeater

            if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
            {
                HiddenField hdfCuenta2 = (HiddenField)item.FindControl("hdfCuenta2");
                Repeater rpt = (Repeater)item.FindControl("rptDatos");


                var Periodo = ddlAnio.SelectedValue + ddlMes.SelectedValue;


                if (hdfCuenta2 != null)
                {
                    var ListaDato = T004_Gasto_IngresoBLL.LOB_2_Hijos(DrpObra.SelectedValue, Periodo,int.Parse(hdfCuentaAux.Value), int.Parse(hdfCuenta2.Value));
                    var ListaDatoManual = T004_Gasto_IngresoBLL.TraeListaDatoManual(DrpObra.SelectedValue, Periodo, int.Parse(hdfCuentaAux.Value), int.Parse(hdfCuenta2.Value));


                    ListaDato.AddRange(ListaDatoManual);

                    rpt.DataSource = null;
                    rpt.DataSource = ListaDato;
                    rpt.DataBind();
                }


              
            }
        }

        protected void rptDatos_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem item = e.Item;
            Panel pnl = (Panel)item.FindControl("pnlModalGrilla");
            Label lblTipo = (Label)item.FindControl("lblTipo");
            HiddenField hdfTipo = (HiddenField)item.FindControl("hdfTipo");
            HiddenField hdfFolio = (HiddenField)item.FindControl("hdfFolio");

            switch (e.CommandName)
            {
                case "Abrir":
                    pnl.Visible = true;
                    if (hdfTipo.Value == "Orden")
                    {
                        lblTipo.Text = "ORDEN DE COMPRA N°:";
                    }
                    else
                    {
                        if (hdfTipo.Value == "Rendicion")
                        {
                            lblTipo.Text = "RENDICION:";
                        }
                        else
                        {
                            lblTipo.Text = "Provision";
                        }
                    }
                    break;
                case "Provisiones":
                    pnlProvision1.Visible = true;
                    pnlProvision2.Visible = true;
                    break;


             
                case "Cerrar":
                    pnl.Visible = false;
                    break;
                case "Descargar":
                   
                    var objAdjunta= T006_Adjunto_ProvisionesBLL.TraerAdjuntoFolio(int.Parse(hdfFolio.Value));
                    string ruta = System.Configuration.ConfigurationManager.AppSettings["Raiz"].ToString();
                    string filePathFinal = ruta + @"Adjunto\" + objAdjunta.IdProvision + "/";
                    Response.Clear();
                    Response.AddHeader("Content-Disposition",
                               "attachment; filename=" + objAdjunta.NombreArchivo);
                    Response.ContentType = "application/octet-stream";
                    Response.WriteFile(filePathFinal+ objAdjunta.NombreArchivo);
                    Response.End();
                    break;
                default:
                    break;
            }

            

        }

        protected void rptDatos_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //btnVer
            //hdfDetalle
            RepeaterItem item = e.Item; // elemento del Repeater

            if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
            {
                HiddenField hdfDetalle = (HiddenField)item.FindControl("hdfDetalle");
                LinkButton btnVer = (LinkButton)item.FindControl("btnVer");
                HiddenField hdfTipo = (HiddenField)item.FindControl("hdfTipo");
                LinkButton btnDescargarAdjunto = (LinkButton)item.FindControl("btnDescargarAdjunto");
                HiddenField hdfFolio = (HiddenField)item.FindControl("hdfFolio");

                if (hdfTipo.Value == "Provision")
                {
                    var objAdjunta = T006_Adjunto_ProvisionesBLL.TraerAdjuntoFolio(int.Parse(hdfFolio.Value));
                    if (objAdjunta != null)
                    {
                        btnDescargarAdjunto.Visible = true;
                    }
                    else
                    {
                        btnDescargarAdjunto.Visible = false;
                    }
                    
                }


                if (hdfDetalle.Value == "+")
                {
                    btnVer.Visible = true;
                }



            }
        }

        protected void rptGrillasDinamicas_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {

                case "Provisiones":
                    RepeaterItem item = e.Item;
                    HiddenField hdfIns = (HiddenField)item.FindControl("hdfInsumo");
                    pnlProvision1.Visible = true;
                    pnlProvision2.Visible = true;
                    hdfInsumoSeleccionado.Value = hdfIns.Value;
                    CargarDatosProvisiones();
                    
                    break;



              
            }
        }

        private void CargarDatosProvisiones()
        {
            string resultado = "";
            string[] insumo = hdfInsumoSeleccionado.Value.Split(' ');
            for (int i = 1; i < insumo.Length; i++)
            {
                resultado += insumo[i] + " ";
            }
               
            
            lblCuenta.Text = hdfObraSeleccionada.Value;
            lblInsumo.Text = resultado;
        }

        protected void btnGuardarProvision_Click(object sender, EventArgs e)
        {

            if (txtGlosa.Text == string.Empty)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alert('Glosa requerida')", true);
                return;
            }
            if (txtProvision.Text == string.Empty)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alert('Provision requerida')", true);
                return;
            }
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    string resultado = "";
                   
                    string[] insumo = hdfInsumoSeleccionado.Value.Split(' ');
                    for (int i = 1; i < insumo.Length; i++)
                    {
                        resultado += insumo[i] + " ";
                    }


                    G005_Base_Datos_Provisiones objBase = new G005_Base_Datos_Provisiones();
                    objBase.Cuenta = insumo[0];
                    objBase.Cuenta_Costo = hdfCuentaAux.Value;
                    objBase.DatoGlosa = txtGlosa.Text;
                    objBase.DatoProvision = int.Parse(Validar.QuitarPunto(txtProvision.Text));
                    objBase.Empresa = "0201";                
                    objBase.Insumo = resultado;
                   
                    objBase.Obra = DrpObra.SelectedValue;
                    objBase.Periodo = ddlAnio.SelectedValue + ddlMes.SelectedValue;
                    objBase.TipoDocumento = "Provision manual";
                    objBase.Glosa = "En espera de aprobacion";

                    long folio = T005_Base_Datos_ProvisionesBLL.Guardar_Provision_Manual(objBase);
                    if (folio >0)
                    {
                        if (pnlAdjunto.Visible )
                        {
                            
                          
                                if (fileupAdjunto.HasFile)
                                {
                                G006_Adjunto_Provisiones objAdjunto = new G006_Adjunto_Provisiones();

                                objAdjunto.IdProvision = folio;

                                objAdjunto.Mime = fileupAdjunto.FileName;
                                objAdjunto.NombreArchivo = Path.GetExtension(fileupAdjunto.FileName);
                                long adjunto = T006_Adjunto_ProvisionesBLL.Guardar_Adjunto(objAdjunto);


                                string ruta = System.Configuration.ConfigurationManager.AppSettings["Raiz"].ToString();

                                   

                                    string filePathFinal = ruta + @"Adjunto\" + folio + "/";
                                   
                                    string nombre_archivo = fileupAdjunto.FileName.ToString();

                                    if (File.Exists(filePathFinal + nombre_archivo.Trim()))
                                    {
                                        File.Delete(filePathFinal + nombre_archivo.Trim());
                                    }

                                    Directory.CreateDirectory(filePathFinal);

                       

                                fileupAdjunto.SaveAs(@filePathFinal + nombre_archivo.Trim());
                          

                                  
                                

                                   
                                }
                        

                        }
                        
                        scope.Complete();
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alert('Provision manual creada')", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), Guid.NewGuid().ToString(), "alert('Error')", true);
                    }
                }

                LimpiarLuegoDeAgregarProvision();

            }
            catch (Exception ex)
            {
               

            }
        }

        private void LimpiarLuegoDeAgregarProvision()
        {
            txtGlosa.Text = string.Empty;
            txtProvision.Text = string.Empty;
            CargarLob1();
            pnlVerObra.Visible = false;
            pnlProvision1.Visible = false;
            pnlProvision2.Visible = false;
        }

        protected void btnAdjunto_Click(object sender, EventArgs e)
        {
            if (pnlAdjunto.Visible)
            {
                pnlAdjunto.Visible = false;
            }
            else
            {
                pnlAdjunto.Visible = true;
            }
        }

        protected void btnDescargarSabana_Click(object sender, EventArgs e)
        {
            DescargarExcel();
        }

        protected void btnCerrarProvision_Click(object sender, EventArgs e)
        {
            pnlProvision1.Visible = false;
            pnlProvision2.Visible = false;
        }
    }
}