﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LibroDeObra.aspx.cs" Inherits="AODigital.Web.LibroDeObra" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    
</head>
<body>
    <link href="css/LibrodeObras.css" rel="stylesheet" />
    <link href="css/adminlte.min.css" rel="stylesheet" />
    <link href="css/style-contenedor.css" rel="stylesheet" />
    <script src="js/jquery-3.3.1.min.js"></script>

    <script src="js/Funciones.js"></script>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.min.css" rel="stylesheet"/>

 



    
    <form id="form1" runat="server">

        <%-- <asp:ScriptManager runat="server" ID="ScriptManagerPrincipal" EnableScriptGlobalization="false"></asp:ScriptManager>
               <asp:UpdatePanel ID="udp_Region" runat="server">
                <ContentTemplate>--%>
                         
		<div class="select-obra">
		   <table class="table-registro">
			   <tbody>
				   <tr>
					  <td>Obra:</td>
					  <td>
						
                          <asp:HiddenField runat="server" ID="hdfObraSeleccionada"/>
                          <asp:HiddenField ID="hdfInsumoSeleccionado" runat="server"  />
						  <asp:DropDownList ID="DrpObra" runat="server" CssClass="form-control" OnSelectedIndexChanged="DrpObra_SelectedIndexChanged" AutoPostBack="true">
								<asp:ListItem Enabled="false">Seleccione una obra</asp:ListItem>
						  </asp:DropDownList>
					  </td>
					   <td><div class="div-periodo-select">Periodo:      
						 <asp:DropDownList ID="ddlMes" runat="server" OnSelectedIndexChanged="DrpObra_SelectedIndexChanged" AutoPostBack="true">
								<asp:ListItem Value="01" Text="Enero"></asp:ListItem>
                             <asp:ListItem Value="02" Text="Febrero"></asp:ListItem>
                             <asp:ListItem Value="03" Text="Marzo"></asp:ListItem>
                             <asp:ListItem Value="04" Text="Abril"></asp:ListItem>
                             <asp:ListItem Value="05" Text="Mayo"></asp:ListItem>
                             <asp:ListItem Value="06" Text="Junio"></asp:ListItem>
                             <asp:ListItem Value="07" Text="Julio"></asp:ListItem>
                             <asp:ListItem Value="08" Text="Agosto" Selected="True"></asp:ListItem>
                             <asp:ListItem Value="09" Text="Septiembre"></asp:ListItem>
                             <asp:ListItem Value="10" Text="Octubre"></asp:ListItem>
                             <asp:ListItem Value="11" Text="Noviembre"></asp:ListItem>
                             <asp:ListItem Value="12" Text="Diciembre"></asp:ListItem>
                           
                        

						  </asp:DropDownList>
                            <asp:DropDownList ID="ddlAnio" runat="server"  OnSelectedIndexChanged="DrpObra_SelectedIndexChanged" AutoPostBack="true">
								<asp:ListItem Value="2021" Text="2021"></asp:ListItem>
                                <asp:ListItem Value="2022" Text="2022"></asp:ListItem>
                                <asp:ListItem Value="2023" Text="2023"></asp:ListItem>
						  </asp:DropDownList>
						</div>

                         

					   </td>
				   </tr>
				</tbody>
		   </table>
		</div>

       <section class="content">
		<div class="container-fluid">
			<div id="MainContent_ctl00">
                <div class="row row-excel">
                    <asp:LinkButton ID="btnDescargarSabana" Visible="false" class="btn-descargar-excel" OnClick="btnDescargarSabana_Click" runat="server">
                        Descargar
                        <img src="img/icon-excel.png" alt="Descargar excel" />

                    </asp:LinkButton>
                  
                </div>
				<div class="row">

					<div class="col-sm-12 col-md-4 col-lg-4">
						<div id="MainContent_P_EnGestion">
							<div class="card card-izq card-danger">
								<div class="card-body">
									<div class="form-horizontal">
										 
										<div>
                                            <asp:HiddenField ID="hdfCuenta" runat="server" />
											<table class="tabla-izquierda">
												<tbody>
                                                    <asp:HiddenField ID="hdfCuentaAux" runat="server" />
                                                      <asp:Repeater runat="server" ID="rptLOB1" OnItemDataBound="rptLOB1_ItemDataBound" OnItemCommand="rptLOB1_ItemCommand">
                                                        <ItemTemplate>   
                                                             <asp:HiddenField ID="hdfCuenta" runat="server" Value='<%# Eval("Cuenta") %>' />
                                                             <asp:HiddenField ID="hdfDescripcion" runat="server" Value='<%# Eval("Descripcion") %>' />
                                                            <tr runat="server" id="trLinea">
														<td class="consumos-izq" runat="server" id="tdDes">
                                                             <%# Eval("Nombre_Completo") %>
														</td>
														<td class="valores-izq" runat="server" id="tdTotal">
                                                             <%# Eval("Total") %>
														</td>
														<td class="ver-izq">
                                                            <asp:LinkButton ID="btnVer" runat="server" CommandArgument='<%# Eval("Cuenta")%>' CommandName="Ver" >
                                                                  <img width="12px" src="img/chevron.png" />
                                                                </asp:LinkButton>

														</td>
													</tr>

                                                        </ItemTemplate>
                                                      </asp:Repeater>
                                                  
													
													<tr >
														<td class="consumos-izq"><strong>Resultado</strong></td>
														<td class="valores-izq"><strong>
                                                            <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>

														                        </strong></td>
														<td class="ver-izq">
                                                           
                                                              
                                                            </td>
													</tr>
												
												
												</tbody>
											</table>
                                            <asp:Panel ID="pnlProvision1" runat="server" Visible="false">
                                                <div class="close" >   <div class="div-cerrar-provision"> 
                                                                  <img class="iconcerrar-modal"  src="img/cross.png" />
                                                               
         </div> </div>
                                            	<table class="tabla-izquierda-dos"  >
												<tbody>
													<tr class="dest-folio-izq" >
														<td colspan="2" style="width: 50%;">
                                                            <div class="titulo-provision"> PROVISIÓN</div>
                                                           </td>
														
													</tr>
													<tr class="destacada-izq">
														<td style="width: 50%;">CUENTA COSTO</td>
														<td style="width: 50%;">
                                                            <asp:Label ID="lblCuenta" runat="server" Text=""></asp:Label>
														</td>
													</tr>
													<tr class="destacada-izq">
														<td style="width: 50%;">INSUMO</td>
													<td style="width: 50%;"><asp:Label ID="lblInsumo" runat="server" Text=""></asp:Label></td>
													</tr>
													<tr class="destacada-izq">
														<td style="width: 50%;">PROVISI&Oacute;N $</td>
															 
                                                        <td>
                                                            <asp:TextBox ID="txtProvision" onkeypress="return justNumbers(event);" onkeyup="separador(this)" class="input-provisiones" runat="server"></asp:TextBox>
                                                         </td>
													</tr>
													<tr>
														<td style="width: 50%;     font-weight: 600;">GLOSA</td>
													 
 													</tr>
													<tr class=" ">
														 
                                                        <td colspan="2" >
                                                            <asp:TextBox ID="txtGlosa" class="input-provisiones" style="width:100%;" runat="server"></asp:TextBox>
                                                        </td>
                                                   
													</tr>
                                              
												</tbody>
											</table>
                                                
                                             <asp:Panel ID="pnlProvision2" runat="server" Visible="false">
										<div class="card-footer">

                                          
 

 


											<div class="div-btn-adjuntar">
                                                <asp:LinkButton ID="btnAdjunto" class="btnes-izq btn-uno" OnClick="btnAdjunto_Click" runat="server">ADJUNTOS</asp:LinkButton>
												

                                                <asp:LinkButton ID="btnGuardarProvision" class="btnes-izq btn-dos" OnClick="btnGuardarProvision_Click" runat="server">GUARDAR</asp:LinkButton>
											
											</div>
                                                  <asp:Panel ID="pnlAdjunto" Visible="false" runat="server">
                                                   
													<tr class=" ">
														 
                                                        <td colspan="2" > 
                                                 
                                                            <asp:FileUpload type="file"  CssClass="upload up"   ID="fileupAdjunto" runat="server" />
                                                   
                                                         </td>
                                                   
													</tr>
                                                        </asp:Panel>
										</div>

                                       </asp:Panel>
                                                 </asp:Panel>
										</div>
										 

                                       
									</div>
								</div>
							</div>
						</div>
					</div>

                 
                    <asp:Panel ID="pnlVerObra" class="col-sm-12 col-md-8 col-lg-8" Visible="false" runat="server">
					<div class="col-sm-12 col-md-8 col-lg-12">
						<div id="MainContent_P_NuevoArriendo">
							<div class="card card-primary">
								<div class="form-inline">
									<div class="card-body">
										<div id="MainContent_P_Cabecera">
											<div class="row">
												<div class="tabla-derecha" id="tablaScroll">



                                                    <%-- aca --%>
                                                    <div class="tabla-derecha-uno">
                                                          

                                                           <asp:Repeater runat="server" ID="rptGrillasDinamicas" OnItemDataBound="rptGrillasDinamicas_ItemDataBound" OnItemCommand="rptGrillasDinamicas_ItemCommand" >
                                                        <ItemTemplate>  
                                                             <asp:HiddenField ID="hdfCuenta2" runat="server" Value='<%# Eval("Cuenta2_Id") %>' />
                                                            <asp:HiddenField ID="hdfInsumo" runat="server" Value='<%# Eval("Gosa") %>' />
                                                             <table>
                                                             <tbody>
                                                                           <tr  class="tr-principal-der">
                                                                           
                                                                         <td class="td-btn-provision"><asp:Button ID="btnProvisiones" runat="server" CommandName="Provisiones" style="width:100%;" class="btn-provision btnes-izq" Text="PROVISIÓN"></asp:Button></td>

                                <td colspan="3" class="tipodocumento-1 titulo-1" >
                                   <strong><%# Eval("Gosa") %> </strong>
                                </td>
                                <td class="titulo-1" >
                                  <strong>TOTAL L&Iacute;NEAS:</strong>
                                </td>
                                <td class="titulo-1" style="text-align: right;" >
                                  <strong>
                                      $<%# Eval("Monto") %></strong></td>
                                <td >&nbsp;</td>
                              </tr>
                              <tr class="tr-titulo-der  ">
                                <td class="td-fecha  titulo-1">
                                  FECHA
                                </td>
                                <td class="td-comentario titulo-1">
                                  &nbsp;
                                </td>
                                <td class="tipo-documento-2 titulo-1">
                                  TIPO DOCUMENTO
                                </td>
                                <td class="td-numero titulo-1">
                                  N&Uacute;MERO
                                </td>
                                <td class="td-glosa titulo-1">
                                  GLOSA
                                </td>
                                <td class="td-monto titulo-1 text-right">
                                  &nbsp;MONTO
                                </td>
                                <td class="td-icon-mas">&nbsp;</td>
                              </tr>

                                                       
                                                
                                                                 <asp:Repeater ID="rptDatos" runat="server" OnItemDataBound="rptDatos_ItemDataBound" OnItemCommand="rptDatos_ItemCommand">
                                                                     <ItemTemplate>  

                                                                                            <tr style="text-align: center;">

 <asp:HiddenField ID="hdfDetalle" runat="server" Value='<%# Eval("Detalle") %>' />

                                <td>
                                  <%# Eval("Fecha") %>
                                </td>
                                <td>
                                    <%# Eval("Movimiento") %>
                                </td>
                                <td>
                                  <%# Eval("Tipo_Documento") %>
                                </td>
                                <td>
                                  <%# Eval("Numero") %>
                                </td>
                                <td ">
                                 <%# Eval("Gosa") %>
                                </td>
                                <td class="valores-izq  text-right">
                                 <%# Eval("Monto") %>
                                </td>
                                <td class="text-center">
                              <asp:LinkButton ID="btnVer"  runat="server" CommandName="Abrir" Visible="false">
                                                                  <img width="8px" src="img/mas.png" />
                                                                </asp:LinkButton>

                                   

                <%--                    <asp:Panel ID="pnlModalGrilla" runat="server" Visible="false" class="GDmodal">
                                        <label class="GDmodal__bg">

                                        </label>
<div class="GDmodal__inner GDmodal__inner2"><label class="GDmodal__close"></label>
<div class="GDtitulosbox">AAA</div>
<div class="GDfondo">
<div class="txt_condiciones"><strong>Vigencia:</strong> AAA 2021</div>
</div>
<br>
<div class="GDfondo">
<div class="txt_condiciones"><strong>Condiciones:</strong></div>
<div>
 <div>AAA</div>
</div>
<br>
</div>
</div></asp:Panel>--%>

<!-- Modal -->
<asp:Panel ID="pnlModalGrilla" runat="server" Visible="false">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
     <asp:LinkButton ID="btnCerrar" class="close" runat="server" CommandName="Cerrar">   <div> 
                                                                  <img class="iconcerrar-modal"  src="img/cross.png" />
                                                               
         </div> </asp:LinkButton>
      <div class="modal-header header-modal-obra  ">

    
          <div class="titulo-modal">

              <div class="t-detalles-modal"><span class="t-span-modal">NUMERO:</span> <%# Eval("Numero") %></div>
                 <div class="t-detalles-modal"> <span class="t-span-modal">GLOSA:</span> <%# Eval("Gosa") %> </div>
               <div class="t-detalles-modal"><span class="t-span-modal">MONTO:</span> <%# Eval("Monto") %></div>
          </div>
      </div>
      <div class="modal-body">
          <table style="border-collapse: collapse; width: 100%;" border="1">
<tbody>
<tr>
<td class="td-modal-destacado">
    <asp:HiddenField ID="hdfTipo" runat="server" Value='<%# Eval("TipoOrdRen") %>' />
 <asp:HiddenField ID="hdfFolio" runat="server" Value='<%# Eval("Numero") %>' />
    <asp:Label ID="lblTipo" runat="server" Text=""></asp:Label>
</td>
<td >  <%# Eval("NUMOC") %>
   
        <asp:LinkButton ID="btnDescargarAdjunto" Visible="false" CommandName="Descargar" runat="server">Descargar</asp:LinkButton>
    

</td>
</tr>
<tr>
<td class="td-modal-destacado">GLOSA:</td>
<td  ><%# Eval("NOMOC") %></td>
</tr>
</tbody>
</table>
    

 

      </div>
     
    </div>
  </div>
</asp:Panel>


                                </td>
                              </tr>



                                                                         </ItemTemplate>

                                                                 </asp:Repeater>                               
           
                                  

                                                                 </tbody>
                                                        </table>



                                                            </ItemTemplate>
                                                            </asp:Repeater>
                                                       
      
                                                        
                                                    </div>
                         
                           
                        </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                        </asp:Panel>


                   
<%--  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header header-modal-obra">
          Título
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <table style="border-collapse: collapse; width: 100%;" border="1">
<tbody>
<tr>
<td class="td-modal-destacado">ORDEN DE COMPRA N°:</td>
<td > F199-7028</td>
</tr>
<tr>
<td class="td-modal-destacado">GLOSA:</td>
<td  >CUSTODIA DE MAQUINAS EST. BIO BIO </td>
</tr>
</tbody>
</table>
    

 

      </div>
     
    </div>
  </div>--%>



				</div>
			</div>
		</div>
	</section>
       <%-- </ContentTemplate>
            </asp:UpdatePanel>--%>

    </form>
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>
        <script src="js/datra.js"></script>
</body>
</html>
