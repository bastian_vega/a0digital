﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CorrecionDeLibro.aspx.cs" Inherits="AODigital.Web.CorrecionDeLibro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
     <link href="css/bootstrap.min.css" rel="stylesheet"/>
     <link href="css/plugins/AdminLTE/adminlte.min.css" rel="stylesheet"/>
    <link href="css/style-contenedor.css" rel="stylesheet"/>
    <link href="css/provisiones.css" rel="stylesheet"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="body-provisiones">
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="main-wrap">

                    <input   id="slide-sidebar" role="button" type="checkbox"> <label class="cerrar " for="slide-sidebar"><span data-tip="Expandir - Reducir"> <img width="15px" src="img/expandir.png"></span></label>
                  
                    <div class="sidebar card card-danger">
                        <form class="div-buscador">
                            <input class="input-buscador" name="busquedacodigo" type="search"> <input class="buscador" type="submit" value="BUSCAR (FACT/REND)">
                        </form>
                        <div>
                            <table class="tabla-slider">
                                <tbody>
                                    <tr class="tr-principal-der">
                                        <td colspan="4"><strong>Elementos de Seguridad</strong></td>
                                        <td><strong>TOTAL L&Iacute;NEAS:</strong></td>
                                        <td><strong>$222.070</strong></td>
                                    </tr>
                                    <tr class="tr-titulo-der">
                                        <td></td>
                                        <td>FECHA</td>
                                        <td>TIPO DOCUMENTO</td>
                                        <td>N&Uacute;MERO</td>
                                        <td>GLOSA</td>
                                        <td>&nbsp;MONTO</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>27-ago</td>
                                        <td>Fact.Elect.con OC</td>
                                        <td>460697</td>
                                        <td>762992442 PROTEKNICA S.A.</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp; 124.000</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>11-ago</td>
                                        <td>Fact.Elect.con OC</td>
                                        <td>445114</td>
                                        <td>767567685 Seguridad Industria</td>
                                        <td>7.770</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>19-ago</td>
                                        <td>Fact.Elect.con OC</td>
                                        <td>2348287</td>
                                        <td>868872004 Comerc.de Art. de P</td>
                                        <td>90.300</td>
                                    </tr>
                                    <tr class="tr-principal-der">
                                        <td colspan="4" style=""><strong>Combustibles y Lubricantes Gen&eacute;rico</strong></td>
                                        <td><strong>TOTAL L&Iacute;NEAS:</strong></td>
                                        <td><strong>$2.873.919</strong></td>
                                    </tr>
                                    <tr class="tr-titulo-der">
                                        <td></td>
                                        <td>FECHA</td>
                                        <td>TIPO DOCUMENTO</td>
                                        <td>N&Uacute;MERO</td>
                                        <td>GLOSA</td>
                                        <td>&nbsp;MONTO</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>02-ago</td>
                                        <td>Fact.Elect.con OC</td>
                                        <td>177</td>
                                        <td>766701833 RV Seguridad Spa</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp; 250.000</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>01-ago</td>
                                        <td>Multiobra/Fact.Elect.</td>
                                        <td>12509524</td>
                                        <td>995200007 Cia.Petroleos Chile</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp; 260.605</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>02-ago</td>
                                        <td>Multiobra/Fact.Elect.</td>
                                        <td>12520420</td>
                                        <td>995200007 Cia.Petroleos Chile</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp; 760.315</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>17-ago</td>
                                        <td>Multiobra/Fact.Elect.</td>
                                        <td>12596536</td>
                                        <td>995200007 Cia.Petroleos Chile</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp; 171.420</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>18-ago</td>
                                        <td>Multiobra/Fact.Elect.</td>
                                        <td>12530205</td>
                                        <td>995200007 Cia.Petroleos Chile</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp; 545.247</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>20-ago</td>
                                        <td>Multiobra/Fact.Elect.</td>
                                        <td>12533945</td>
                                        <td>995200007 Cia.Petroleos Chile</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp; 444.291</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>06-ago</td>
                                        <td>Rendici&oacute;n</td>
                                        <td>REND: 7823</td>
                                        <td>045213501 Crisologo&nbsp; Mart&iacute;nez</td>
                                        <td>37.800</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>13-ago</td>
                                        <td>Rendici&oacute;n</td>
                                        <td>REND: 7849</td>
                                        <td>045213501 Crisologo&nbsp; Mart&iacute;nez</td>
                                        <td>37.800</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>20-ago</td>
                                        <td>Rendici&oacute;n</td>
                                        <td>REND: 7887</td>
                                        <td>045213501 Crisologo&nbsp; Mart&iacute;nez</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp; 154.800</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>27-ago</td>
                                        <td>Rendici&oacute;n</td>
                                        <td>REND: 7911</td>
                                        <td>045213501 Crisologo&nbsp; Mart&iacute;nez</td>
                                        <td>82.812</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>06-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>REND: 7830</td>
                                        <td>764454863 COM INVERSIONES Y T</td>
                                        <td>13.277</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>06-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>68452</td>
                                        <td>764454863 COM INVERSIONES Y T</td>
                                        <td>17.727</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>13-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>68487</td>
                                        <td>764454863 COM INVERSIONES Y T</td>
                                        <td>8.869</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>13-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>68636</td>
                                        <td>764454863 COM INVERSIONES Y T</td>
                                        <td>13.296</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>20-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>68986</td>
                                        <td>764454863 COM INVERSIONES Y T</td>
                                        <td>13.221</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>20-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>138166</td>
                                        <td>761432788 Serrano y Melo Dist</td>
                                        <td>16.746</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>20-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>228045</td>
                                        <td>771958206 Com. Monserrat Limi</td>
                                        <td>13.194</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>27-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>69293</td>
                                        <td>764454863 COM INVERSIONES Y T</td>
                                        <td>8.784</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>27-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>69344</td>
                                        <td>764454863 COM INVERSIONES Y T</td>
                                        <td>10.541</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>27-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>229387</td>
                                        <td>771958206 Com. Monserrat Limi</td>
                                        <td>13.174</td>
                                    </tr>
                                    <tr class="tr-principal-der">
                                        <td colspan="4" style=""><strong>Herramientas y Enseres Gen&eacute;rico</strong></td>
                                        <td><strong>TOTAL L&Iacute;NEAS:</strong></td>
                                        <td><strong>$1.537.625</strong></td>
                                    </tr>
                                    <tr class="tr-titulo-der">
                                        <td></td>
                                        <td>FECHA</td>
                                        <td>TIPO DOCUMENTO</td>
                                        <td>N&Uacute;MERO</td>
                                        <td>GLOSA</td>
                                        <td>&nbsp;MONTO</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>23-ago</td>
                                        <td>CL-Manual</td>
                                        <td>Cobro de insumos a obras</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp; 915.000</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>23-ago</td>
                                        <td>Fact.Elect.</td>
                                        <td>7067</td>
                                        <td>054209835 Miguel Castro Azocar</td>
                                        <td>27.647</td>
                                        <td style="width: 32.2049px; height: 0px;">&nbsp;</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>03-ago</td>
                                        <td>Fact.Elect.con OC</td>
                                        <td>2503826</td>
                                        <td>832850004 Abasolo y Compania</td>
                                        <td>31.596</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>04-ago</td>
                                        <td>Fact.Elect.con OC</td>
                                        <td>2503822</td>
                                        <td>832850004 Abasolo y Compania</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp; 104.706</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>10-ago</td>
                                        <td>Fact.Elect.con OC</td>
                                        <td>17151</td>
                                        <td>765953286 Comerc. Rosamel Ric</td>
                                        <td>30.000</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>17-ago</td>
                                        <td>Fact.Elect.con OC</td>
                                        <td>2506301</td>
                                        <td>832850004 Abasolo y Compania</td>
                                        <td>48.067</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>19-ago</td>
                                        <td>Fact.Elect.con OC</td>
                                        <td>4527552</td>
                                        <td>963550006 Electrocom S.A.</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp; 260.275</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>26-ago</td>
                                        <td>Fact.Elect.con OC</td>
                                        <td>22869</td>
                                        <td>763189546 COMERCIAL DECARLI Y</td>
                                        <td>38.000</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>06-ago</td>
                                        <td>Rendici&oacute;n</td>
                                        <td>REND: 7823</td>
                                        <td>045213501 Crisologo&nbsp; Mart&iacute;nez</td>
                                        <td>3.400</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>06-ago</td>
                                        <td>Rendici&oacute;n</td>
                                        <td>REND: 7830</td>
                                        <td>09087358K Luis&nbsp; Zambrano Leal</td>
                                        <td>2.000</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>13-ago</td>
                                        <td>Rendici&oacute;n</td>
                                        <td>REND: 7849</td>
                                        <td>045213501 Crisologo&nbsp; Mart&iacute;nez</td>
                                        <td>6.590</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>13-ago</td>
                                        <td>Rendici&oacute;n</td>
                                        <td>REND: 7894</td>
                                        <td>09087358K Luis&nbsp; Zambrano Leal</td>
                                        <td>2.350</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>27-ago</td>
                                        <td>Rendici&oacute;n</td>
                                        <td>REND: 7911</td>
                                        <td>045213501 Crisologo&nbsp; Mart&iacute;nez</td>
                                        <td>1.000</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>27-ago</td>
                                        <td>Rendici&oacute;n</td>
                                        <td>REND: 7913</td>
                                        <td>09087358K Luis&nbsp; Zambrano Leal</td>
                                        <td>5.490</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>16-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>24849798</td>
                                        <td>765686601 Easy Retail S.A.</td>
                                        <td>18.479</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>20-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>76</td>
                                        <td>07616732K JOSE DAVID TORRES U</td>
                                        <td>12.185</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>20-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>8546</td>
                                        <td>083636920 Luis Martinez Rocha</td>
                                        <td>14.538</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>20-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>22306</td>
                                        <td>135106410 Maria Duran Carrasc</td>
                                        <td>6.218</td>
                                    </tr>
                                    <tr style="text-align: center;">
                                        <td><input class="check-tabla" type="checkbox"></td>
                                        <td>20-ago</td>
                                        <td>Rendicion/Fact.Elect</td>
                                        <td>141376</td>
                                        <td>786607906 Import. y Distrib.</td>
                                        <td>10.084</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="portfolio">
                        <div class="cont-derech">
                            <div class="cont-der-1 resizable">
                                <div class="content-derecho-head">
                                    <form class="select-derecho-arriba">
                                        <select>
                                            <option value=" ">
                                                TRASPASO COSTOS
                                            </option>
                                            <option value=" ">
                                                TRASPASO COSTOS 1
                                            </option>
                                            <option value=" ">
                                                TRASPASO COSTOS 2
                                            </option>

                                        </select><br>
                                        <br>
                                    </form>
                                    <div class="solicitud">
                                        SOLICITUD N° 2222
                                    </div>
                                </div>
                                <div>
                                    <table class="tablas-derecho">
                                        <tbody>
                                            <tr class="tr-principal-der">
                                                <td width="30%" class="text-center">FACT/REN N&deg;</td>
                                                <td width="30%" class="text-center">INSUMO</td>
                                                <td width="30%" class="text-center">MONTO</td>
                                            </tr>
                                            <tr class="text-center">
                                                <td>177</td>
                                                <td>Subcontratos de Servicios Gen&eacute;rico</td>
                                                <td>2.700.000</td>
                                            </tr>
                                            <tr class="text-center">
                                                <td>177</td>
                                                <td>Arriendos Maquinaria</td>
                                                <td>2.700.000</td>
                                            </tr>
                                            <tr class="text-center">
                                                <td>7911</td>
                                                <td>Arriendos Maquinaria</td>
                                                <td>2.700.000</td>
                                            </tr>
                                            <tr class="text-center">
                                                <td>7911</td>
                                                <td>Pensiones;Viajes y Visitas Obr Gen&eacute;rico</td>
                                                <td>2.700.000</td>
                                            </tr>
                                            <tr class="text-center tr-titulo-der">
                                                <td colspan="2">
                                                    MONTO TOTAL  A REPARTIR        </td>
                                                <td>
                                                    $ 9.285.660
                                                </td>
                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="col-8">

                                        <div class="obras">
                                            DEFINA OBRAS A TRASPASAR COSTOS <input class="input-numeros" type="number"   value="42"  
                                                                                   min="2" max="2">
                                        </div>

                                    </div>

                                    <div class="col-4">
                                        <div class="content-btn-grabar">
                                            <button class="btn-grabar">grabar</button>
                                        </div>
                                    </div>
                                 </div>
                               
                                <div class="row">
                                    <div class="div-detalle-total">
                                        <table class="tabla-slider-derecho" width=" ">
                                            <tbody>
                                                <tr class="tabla-slider-derecho-tr">
                                                    <td>FACT/REN N&deg;</td>
                                                    <td>INSUMO</td>
                                                    <td>&nbsp;MONTO</td>
                                                    <td rowspan="7">&nbsp;</td>
                                                    <td>
                                                         
                                                            
                                                            <input class="search__input" type="text" placeholder="OBRA 1">
                                                        
                                                    </td>
                                                    <td>
                                                       
                                                    <button class="ver-obra">OBRA 2 <i class="fa fa-search" aria-hidden="true"></i></button></td>
                                                    <td><button class="ver-obra">OBRA 3 <i class="fa fa-search" aria-hidden="true"></i></button></td>
                                                    <td><button class="ver-obra">OBRA 4 <i class="fa fa-search" aria-hidden="true"></i></button></td>
                                                    <td><button class="ver-obra">OBRA 5 <i class="fa fa-search" aria-hidden="true"></i></button></td>
                                                   
                                                    <td><strong>&Sigma;</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>177</td>
                                                    <td>99404</td>
                                                    <td>2.700.000</td>
                                                    <td> 270.000</td>
                                                    <td> 810.000</td>
                                                    <td> 810.000</td>
                                                    <td> 270.000</td>
                                                    <td> 540.000</td>
                                                    <td>0</td>
                                                </tr>
                                                <tr>
                                                    <td>177</td>
                                                    <td>99501</td>
                                                    <td>250.000</td>
                                                    <td> 25.000</td>
                                                    <td> 75.000</td>
                                                    <td> 75.000</td>
                                                    <td> 25.000</td>
                                                    <td> 50.000</td>
                                                    <td>0</td>
                                                </tr>
                                                <tr>
                                                    <td>7911</td>
                                                    <td>99501</td>
                                                    <td>330.000</td>
                                                    <td> 33.000</td>
                                                    <td> 99.000</td>
                                                    <td> 99.000</td>
                                                    <td> 33.000</td>
                                                    <td> 66.000</td>
                                                    <td>0</td>
                                                </tr>
                                                <tr>
                                                    <td>7911</td>
                                                    <td>99704</td>
                                                    <td>336.000</td>
                                                    <td> 33.600</td>
                                                    <td>100.800</td>
                                                    <td>100.800</td>
                                                    <td> 33.600</td>
                                                    <td> 67.200</td>
                                                    <td>0</td>
                                                </tr>
                                                <tr>
                                                    <td>REMUNE</td>
                                                    <td>99202</td>
                                                    <td>2.995.813</td>
                                                    <td>299.581</td>
                                                    <td>898.744</td>
                                                    <td>898.744</td>
                                                    <td>299.581</td>
                                                    <td>599.163</td>
                                                    <td>0</td>
                                                </tr>
                                                <tr>
                                                    <td>REMUNE</td>
                                                    <td>99202</td>
                                                    <td>2.673.847</td>
                                                    <td>267.385</td>
                                                    <td>802.154</td>
                                                    <td>802.154</td>
                                                    <td>267.385</td>
                                                    <td>534.769</td>
                                                    <td>0</td>
                                                </tr>
                                              
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td><strong>TOTAL</strong></td>
                                                    <td>&nbsp;</td>
                                                    <td><strong>928.566 </strong></td>
                                                    <td><strong>2.785.698 </strong></td>
                                                    <td><strong>2.785.698 </strong></td>
                                                    <td><strong>928.566 </strong></td>
                                                    <td><strong>1.857.132 </strong></td>
                                                    <td>&nbsp;</td>
                                                </tr> 
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td><strong>INSUMO</strong></td>
                                                    <td>&nbsp;</td>
                                                    <td><strong>99712</strong></td>
                                                    <td><strong>99712</strong></td>
                                                    <td><strong>99712</strong></td>
                                                    <td><strong>99712</strong></td>
                                                    <td><strong>99712</strong></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="comentario">
                                                    <strong> Comentario: </strong> Costos traspasados por traspaso de Personal mas camioneta por 15 días.
                                                </div>
                                            </div>
                                            <div class="col-6  text-center"">

                                                <div class="btnes-izq btn-uno">
                                                    ADJUNTOS <i class="fa fa-paperclip" aria-hidden="true"></i>

                                                </div>
                                            </div>
                                             <div class="col-6  text-center">
                                                    <div class="btnes-izq btn-dos">
                                                        GUARDAR
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="cont-der-2">
                                <div class="movi-traspasar">
                                    Seleccione uno o más movimientos a <strong>“Traspasar”</strong>
                                </div>
                                <div>
                                    <div class="detalle-derech">
                                        <span>DETALLE:</span> FACTURA 177  |  766701833 RV Seguridad Spa   |  OC F199-7028 CUSTODIA DE MAQUINAS EST. BIO BIO
                                    </div>
                                </div>
                                <div class="elemento-traspasar">
                                    <table class="tablas-derecho">
                                        <tbody>
                                            <tr class="tr-principal-der text-center ">
                                                <td>SELECCIÓN</td>
                                                <td>INSUMO</td>
                                                <td>&nbsp;MONTO</td>
                                            </tr>
                                            <tr class="text-center">
                                                <td><input class="check-tabla" type="checkbox"></td>
                                                <td>Subcontratos de Servicios Gen&eacute;rico</td>
                                                <td>2.700.000</td>
                                            </tr>
                                            <tr class="text-center">
                                                <td><input class="check-tabla" type="checkbox"></td>
                                                <td>Arriendos Maquinaria</td>
                                                <td>250.000</td>
                                            </tr>
                                            <tr class="text-center">
                                                <td><input class="check-tabla" type="checkbox"></td>
                                                <td>Combustibles y Lubricantes Gen&eacute;rico</td>
                                                <td>250.000</td>
                                            </tr>
                                            <tr class="text-center">
                                                <td><input class="check-tabla" type="checkbox"></td>
                                                <td>Varios Gastos Generales Gen&eacute;rico</td>
                                                <td>150.000</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="row">
                                    <div class="col-8">

                                      

                                    </div>

                                    <div class="col-4">
                                        <div class="content-btn-grabar">
                                            <button class="btn-grabar">Insertar</button>
                                        </div>
                                    </div>
                                </div>
                             
                            </div>

                        </div>
                    </div>
                </div>
                </div>
            </div><!-- partial -->
        </section>
    </div>
    </form>
</body>
</html>
