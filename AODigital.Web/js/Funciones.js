﻿function checkRut(rut) {
    // Despejar Puntos
    if (rut.value != "") {
        var valor = rut.value.replace('.', '');
        valor = valor.replace('.', '');
        // Despejar Guión
        valor = valor.replace('-', '');

        // Aislar Cuerpo y Dígito Verificador
        if (valor.length > 7) {
            var cuerpo = valor.slice("0", -1);
            var dv = valor.slice(-1).toUpperCase();
        }
        else {
            var cuerpo = valor;
        }
        // Formatear RUN
        if (cuerpo.length < 7) {
            
            $(rut).val("");
            //return false;
        }

        // Calcular Dígito Verificador
        var suma = 0;
        var multiplo = 2;

        // Para cada dígito del Cuerpo
        for (var i = 1; i <= cuerpo.length; i++) {

            // Obtener su Producto con el Múltiplo Correspondiente
            var index = multiplo * valor.charAt(cuerpo.length - i);

            // Sumar al Contador General
            suma = suma + index;

            // Consolidar Múltiplo dentro del rango [2,7]
            if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

        }

        // Calcular Dígito Verificador en base al Módulo 11
        var dvEsperado = 11 - (suma % 11);

        // Casos Especiales (0 y K)
        var dv = (dv == 'K') ? 10 : dv;
        dv = (dv == 0) ? 11 : dv;

        // Validar que el Cuerpo coincide con su Dígito Verificador
        if (dvEsperado != dv) {
            $(rut).val("");
        }
    }
    else {
        $(rut).val("");
    }
    if (isAndroidExplorer()) {
        formatRut(rut);
    }
}


function alfanumericos(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz1234567890";
    especiales = [8, 127, 32];

    tecla_especial = false;
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) return false;
}

function formatRut(rut) {
    if (rut.value != " " && rut.value != "-" && rut.value != "") {
        var cont = 0;
        var format;
        var valor = rut.value.replace('.', '');// Quitar el primer punto de rut
        valor = valor.replace('.', '');//Quitar el segundo punto del rut
        valor = valor.replace('-', '');//Quitar guion
        valor = valor.replace(' ', '');//Quitar espacios en blanco
        format = "-" + valor.substring(valor.length - 1);
        for (var i = valor.length - 2; i >= 0; i--) {
            format = valor.substring(i, i + 1) + format;
            cont++;
            if (cont == 3 && i != 0) {
                format = "." + format;
                cont = 0;
            }
        }
        rut.value = format;
        //$("#searchCustomerID").val(format);

        //$("#Rut").val(format);
    }
    else {
        rut.value = "";
    }
}

function justNumbers(e) {
    var keynum = window.event ? window.event.keyCode : e.which;

    if ((keynum == 8) || (keynum == 46) || (keynum == 45))
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}

// Con letra k para valdiar rut
function justNumbers2(e) {
    var keynum = window.event ? window.event.keyCode : e.which;

    if ((keynum == 8) || (keynum == 46) || (keynum == 45) || (keynum == 75) || (keynum == 107))
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}

function alfanumericos(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz1234567890";
    especiales = [8, 127, 32];

    tecla_especial = false;
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) return false;
}


function SoloX(e) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if ((keynum == 88) || (keynum == 120))
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}

function SoloCoP(e) {
    var keynum = window.event ? window.event.keyCode : e.which;
    if ((keynum == 80) || (keynum == 112) || (keynum == 67) || (keynum == 99))
        return true;
    return /\d/.test(String.fromCharCode(keynum));
}


function isAndroidExplorer() {
    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
    return isAndroid;
}

function separador(e) {
    $(e).on({
        "focus": function (event) {
            $(event.target).select();
        },
        "keyup": function (event) {
            $(event.target).val(function (index, value) {
                return value.replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
            });
        }
    });
}

function Validate(e) {
    var pattern = /^-?[0-9]+(.[0-9]{1,2})?$/;
    var text = e.value;
    if (text.match(pattern) == null) {
        //alert('Formato incorrecto');
        e.value = '';
    }
    //else
    //{
    //	alert('OK');
    //}
}

